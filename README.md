# Powhegbox

Project collecting the necessary data for running Powhegbox with Gauss.

## [gridpacks](gridpacks)

Gridpacks for Powhegbox generation are stored here. These are the
integration grids generated at initialization, and depend only upon
the Powhegbox version, event type, and beam energies. Note that the
Gauss version and architecture do not change these grids, as there are
no compiled binaries, only text files.

```bash
# Clone this repository.
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/Gen/PowhegboxData.git

# Run the script. This generates the gridpack and moves it to 
# gridpacks/<Powhegbox version>/<Powhegbox revision>.
lb-run -c <CMTCONFIG> Gauss/<version> cmt/gridpack.sh <beam configuration> <event type>

# Add the gridpack and commit.
git checkout -b <event type>
git add gridpacks/<Powhegbox version>
git commit -m "Added gridpack for <event type>."
git push -u origin <event type>

# Make a merge request for the gridpack on gitlab.
```

A working example is given below. Powhegbox has not yet been included
in an official release, and so the following builds Gauss.
```bash
# Set the environment.
# The data path is required, even if empty.
export CMTCONFIG=x86_64-centos7-gcc9-opt
export export POWHEGBOXDATAROOT=
source /cvmfs/lhcb.cern.ch/group_login.sh

# Build.
lb-dev Gauss/v54r2 
cd ./GaussDev_v54r2
git lb-use Gauss
git lb-checkout Gauss/LHCBGAUSS-471.LbPowheg Gen/LbPowheg
make configure
make

# Run, here 'example.py' is the dummy event type.
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/Gen/PowhegboxData.git
./run PowhegboxData/cmt/gridpack.sh '$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py' '$LBPOWHEGROOT/doc/example.py'

# This creates '70000000_6500_6500.tgz'.
ls PowhegboxData/gridpacks/2/3744/

# Remove the run directory to test if things worked.
rm -r 70000000_6500_6500/

# To use this gridpack, the POWHEGBOXDATAROOT path must be set.
export POWHEGBOXDATAROOT=$PWD/PowhegboxData
./run Gen/LbPowheg/doc/example.sh
```
