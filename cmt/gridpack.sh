#!/usr/bin/env bash
# Usage: "./gridpack.sh <beam configuration> <decay file>"

# Determine the top of this package.
TOP=$(dirname "$(readlink -f "$0")")

# Find the datatype.
datatype=`basename $1|cut -d '-' -f3|grep -E ^\-?[0-9]+$`
if [ "$datatype" = "" ]; then
  datatype=`basename $1|cut -d '-' -f4|grep -E ^\-?[0-9]+$`
  if [ "$datatype" = "" ]; then
    datatype='Upgrade'
  fi
fi

# Create the configuration.
CFG=gridpack.py
cat > $CFG << BLOCKTEXT
from Configurables import LHCbApp, OutputStream, Gauss
from Configurables import Generation, Special, PowhegProduction
OutputStream("GaussTape").Output = (
    "DATAFILE='PFN:gridpack.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'")
LHCbApp().EvtMax = 1
Gauss().Phases = ["Generator", "GenToMCTree"]
Gauss().SampleGenerationToolOptions.update({"GenGridpack": True})
Gauss().DataType = "$datatype"
BLOCKTEXT

# Run the generation.
gaudirun.py $1 $LBPOWHEGROOT/options/PowhegPythia8.py $2 $CFG

# Create the zipped tarball and move.
GRID=`ls -td */ | head -n 1`
GRID=${GRID::-1}
tar -czf $GRID.tgz $GRID
mkdir -p $TOP/../gridpacks/$POWHEGBOXVER/
mv $GRID.tgz $TOP/../gridpacks/$POWHEGBOXVER/
